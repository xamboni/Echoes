﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soundwave : MonoBehaviour
{
    [HideInInspector]
    public float maxRadius = 0f;
    [HideInInspector]
    public float travelSpeed = 0f;
    [HideInInspector]
    public Color color;
    [HideInInspector]
    public float thickness = 0f;

    float startTime = 0f;
    float travelTime = 0f;
    Renderer objRenderer;

    GameObject rim;

    bool bStarted = false;
    
    float dist;

    void Start()
    {
        objRenderer = GetComponent<Renderer>();
    }

    
    public void Init()
    {
        startTime = Time.time;
        travelTime = maxRadius / travelSpeed;

        Transform rimTransform = transform.GetChild(0);

        if (rimTransform != null)
        {
            rim = rimTransform.gameObject;

            if (rim != null && objRenderer != null)
                rim.GetComponent<Renderer>().material.SetColor("_Color", objRenderer.material.GetColor("_Color"));
        }

        bStarted = true;
    }

    void OnTriggerEnter(Collider other)
    {
        IHighlight script = other.gameObject.GetComponent<IHighlight>();
        IReacting reactScript = other.gameObject.GetComponent<IReacting>();

        if (script != null)
        {
            script.Highlight(1f, color);
        }

        if (reactScript == null) return;

        reactScript.ReactSound(transform.position);

        if (dist < 0.15f)
        {
            reactScript.Find();
        }
    }

    void Update()
    {
        if (!bStarted)
            return;

        float elapsed = Time.time - startTime;
        float frac = Mathf.Min(elapsed / travelTime, 1f);
        
        dist = frac;

        transform.localScale = Vector3.one * (frac * maxRadius);
        objRenderer.material.SetColor("_Color", new Color(color.r, color.g, color.b, 1f - frac));

        if (rim != null)
        {
            Renderer rimRenderer = rim.GetComponent<Renderer>();
            rimRenderer.material.SetColor("_Color", new Color(color.r, color.g, color.b, 1f - frac));
        }

        if (frac >= 1f)
            Destroy(gameObject);
	}
}
