﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class hearingNPC : MonoBehaviour {

    public float speed;

    bool react = false;
    bool find = false;
    bool walk = false;

    float npcDisabledTime = 10.0f;
    bool npcDisabled = false;

    Vector3 pos;

    float ypos;

    Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
        //ypos = transform.position.y;
        // test wave
        //Util.MakeSoundWave(transform.position, 100f, 50f, new Color(1, 0, 0, 1), 10f);
    }
    /*
    public void ReactSound(Vector3 position){
        pos = position;
        speed = 4;
        pos.y = 0;
        react = true;
        check();
    }

    public void Find()
    {
        find = true;
        check();
    }

    void check()
    {
        //Debug.Log(find);
        if (react && find)
        {
            walk = true;
        }
    }
    */
    private void Update()
    {
        
        if (npcDisabled)
        {
            npcDisabledTime -= Time.deltaTime;
            if(npcDisabledTime < 0)
            { 
                npcDisabled = false;
                npcDisabledTime = 10.0f;
            }
        }
        
        /*
        float step = speed * Time.deltaTime;
        if (walk)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(pos.x, ypos, pos.z), step);

            if (transform.position == new Vector3(pos.x, ypos, pos.z))
            {
                walk = false;
            }
        }
        */
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.GetComponent<BaseProp>() != null)
        {
            anim.SetTrigger("Attack");
            npcDisabled = true;
            Util.MakeSoundWave(transform.position, 100f, 50f, new Color(1, 0, 0, 1), 10f);
        }

        if (collision.gameObject.tag == "Player" && npcDisabled == false)
        {
            anim.SetTrigger("Attack");
            Util.MakeSoundWave(transform.position, 100f, 50f, new Color(1, 0, 0, 1), 10f);
            SceneManager.LoadScene("subway_Emmanuel");
        }
    }
}
