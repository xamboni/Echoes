﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicManager : MonoBehaviour
{

	[SerializeField] private AudioClip[] ambientMusic;
	[SerializeField] private AudioClip winMusic;
	private bool endingMusic;
	private AudioSource source;

	private void PlayNextPassiveSong()
	{
		var clip = ambientMusic[Random.Range(0, ambientMusic.Length)];

		source.clip = clip;
		source.Play();
		Invoke("PlayNextPassiveSong", clip.length);
	}

	private void Update()
	{
		if (source.isPlaying && GameManager.Instance.HasWon && !endingMusic)
		{
			endingMusic = true;
			source.Stop();
			source.clip = winMusic;
			source.Play();
		}
	}

	private void Start()
	{
		
	}

	private void Awake()
	{
		source = GetComponent<AudioSource>();
		PlayNextPassiveSong();
	}
}
