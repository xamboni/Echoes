﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundwaveTester : MonoBehaviour
{

	void Start()
    {
		
	}

    void Awake()
    {

    }

    void Update()
    {
		if (Input.GetMouseButtonDown(0))
        {
            gameObject.layer = 2;
            RaycastHit trace;
            Ray dir = Camera.main.ScreenPointToRay(Input.mousePosition);
            bool bHit = Physics.Raycast(dir, out trace, 16000f);

            if (bHit)
            {
                Util.MakeSoundWave(trace.point, 300f, 100f, new Color(0f, 0.8f, 0f), 10f);
            }

            gameObject.layer = 0;
        }
	}
}
