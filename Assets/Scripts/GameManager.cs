﻿using UnityEngine;

/// <inheritdoc />
/// <summary>
///     GameManager class. Put gamemode-related things here.
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class GameManager : MonoBehaviour
{
	[SerializeField] private GameObject wavePrefab;
	[SerializeField] private GameObject wavePrefabRim;
	[SerializeField] private Material blackMat;
	[SerializeField] private GameObject sobelPrefab;
	[SerializeField] private AudioClip[] stingers;

	private AudioSource audio;

	public bool HasWon;

	private void Start()
	{
		audio = GetComponent<AudioSource>();
	}

	// Can't be set, only retrieved
	public static GameManager Instance { get; private set; }

    /// <summary>
    ///     Makes a soundwave which highlights all intersecting geometry
    /// </summary>
    /// <param name="position">Origin of the effect</param>
    /// <param name="radius">Max radius of the effect</param>
    /// <param name="speed">Expansion speed of the effect</param>
    /// <param name="color">Color of the effect</param>
    /// <param name="thickness">Thickness of the edge</param>
    /// <param name="rimThickness">Thickness of the brighter exterior rim</param>
    public void MakeSoundWave(Vector3 position, float radius, float speed, Color color, float thickness,
	                          float rimThickness = 0.5f)
	{
		if (wavePrefab == null) return;

		var soundwave = Instantiate(rimThickness > 0f ? wavePrefabRim : wavePrefab);

		var script = soundwave.GetComponent<Soundwave>();
		script.maxRadius = radius;
		script.travelSpeed = speed;
		script.color = color;
		script.transform.position = position;
		script.thickness = thickness;
		soundwave.GetComponent<Renderer>().material.SetColor("_Color", color);
		soundwave.GetComponent<Renderer>().material.SetFloat("_Thickness", thickness);

		if (rimThickness > 0f)
			soundwave.transform.GetChild(0).GetComponent<Renderer>().material.SetFloat("_Thickness", rimThickness);

		script.Init();
	}

    /// <summary>
    ///     Creates a localized sobel wave with the given parameters.
    /// </summary>
    /// <param name="position">Origin of the effect</param>
    /// <param name="radius">Max radius of the effect</param>
    /// <param name="speed">Expansion speed of the effect</param>
    /// <param name="color">Color of the effect</param>
    /// <param name="persistTime">Time before the effect begins to fade out</param>
    /// <param name="lifeTime">Time it takes for the effect to fade out</param>
    /// <param name="zTop">If the effect should take priority over all other effects, and be drawn on top</param>
    public void MakeSobelWave(Vector3 position, float radius, float speed, Color color, float persistTime = 3f,
	                          float lifeTime = 5f, bool zTop = false)
	{
		if (sobelPrefab == null) return;

		var soundwave = Instantiate(sobelPrefab);

		var script = soundwave.GetComponent<SobelRing>();
		script.maxRadius = radius;
		script.travelSpeed = speed;
		script.color = color;
		script.transform.position = position;
		script.lifeTime = lifeTime;
		script.persistTime = persistTime;
		script.zTop = zTop;
		soundwave.GetComponent<Renderer>().material.SetColor("_Color", color);

		script.Init();
	}

	// Singleton our GameManager
	private void Awake()
	{
		if (Instance == null)
			Instance = this;
		else if (Instance != this) // We already have a GameManager present, destroy this crap
			Destroy(gameObject);

		// Don't destroy our GameManager if the level reloads
		DontDestroyOnLoad(gameObject);

		// PAINT IT BLACK
		var all = FindObjectsOfType<GameObject>();

		foreach (var obj in all)
		{
			var renderer = obj.GetComponent<Renderer>();

			if (renderer != null)
			{
				var mats = new Material[renderer.materials.Length];
				for (var j = 0; j < renderer.materials.Length; j++) mats[j] = blackMat;
				renderer.materials = mats;
			}
		}

		Camera.main.depthTextureMode |= DepthTextureMode.DepthNormals;
	}

	public void PlayStinger()
	{
		audio.clip = stingers[Random.Range(0, stingers.Length)];
		audio.Play();
	}

	private void Update()
	{
	}
}