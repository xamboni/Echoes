﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public static class Util
{
    public static bool IsValid<T>(T obj)
    {
        return obj != null;
    }

    public static void PlaySound(AudioSource source)
    {
        // TODO: This will call Play() on the given audio source and make a sound wave depending on
        // the settings of the audio source at the time
    }
    
    public static T Choose<T>(ref T[] obj)
    {
        int chosen = Random.Range(0, obj.Length);
        return obj[chosen];
    }
    
    public static T Choose<T>(ref T[] obj, out int chosen)
    {
        chosen = Random.Range(0, obj.Length);
        return obj[chosen];
    }

    public static void MakeSoundWave(Vector3 position, float radius, float speed, Color color, float thickness)
    {
        GameManager.Instance.MakeSoundWave(position, radius, speed, color, thickness);
    }

    public static void MakeSobelWave(Vector3 position, float radius, float speed, Color color, float persistTime = 3f, float lifeTime = 5f, bool zTop = false)
    {
        GameManager.Instance.MakeSobelWave(position, radius, speed, color, persistTime, lifeTime, zTop);
    }
}
