﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWaves : MonoBehaviour
{

    private Vector3 position;

    void Start()
    {
        position = transform.position;
    }

    void Update()
    {
        float distance = (position - transform.position).magnitude;

        if (distance >= 1)
        {
            position = transform.position;

            gameObject.layer = 2;
            
            Util.MakeSobelWave(transform.position, 10f, 100f, new Color(1, 0, 0, 1), 1f, 1f, true);
           
            gameObject.layer = 0;
        }
        
    }
}
