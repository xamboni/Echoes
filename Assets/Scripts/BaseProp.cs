﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(AudioSource))]
public class BaseProp : MonoBehaviour
{
    private AudioSource audio;
    public bool canPickup = true;
    private Collider collider;
    [SerializeField] private AudioClip[] impactSounds;
    private int lastSoundIndex;
    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();
        audio = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.root.CompareTag("Player")) return;

        canPickup = true;
        // this is nasty but i really don't care
        Physics.IgnoreCollision(GetComponent<MeshCollider>(),
            GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterController>(), false);

        if (collision.impulse.magnitude < 1f) return;

        var soundIndex = Random.Range(0, impactSounds.Length);
        while (soundIndex == lastSoundIndex) soundIndex = Random.Range(0, impactSounds.Length);
        lastSoundIndex = soundIndex;

        Util.MakeSobelWave(collision.contacts[0].point, collision.impulse.magnitude * 2.5f, 30f, Color.white, 1.5f,
            2.5f
        );
        audio.clip = impactSounds[soundIndex];
        audio.Play();
    }
}