﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinusScript : MonoBehaviour {

    public GameObject Slider;

    private void OnTriggerEnter(Collider other)
    {
        Slider.GetComponent<Slider>().value -= 0.1f;
    }
}
