﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackScript : MonoBehaviour {
    public GameObject PauseMenu;

    private void OnTriggerEnter(Collider col)
    {
        PauseMenu.GetComponent<PauseMenu>().SettingsButtonExit();
    }
}
