// Upgrade NOTE: upgraded instancing buffer 'IntersectIncr' to new syntax.

// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Intersect Incr"
{
	Properties
	{
		_Color("Color", Color) = (1,0.5704868,0.1102941,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+3003" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Front
		ZWrite Off
		ZTest Always
		Stencil
		{
			Ref 2
			ReadMask 2
			WriteMask 2
			Comp Equal
			Pass Keep
			Fail Keep
			ZFail Replace
		}
		CGPROGRAM
		#pragma target 3.0
		#pragma multi_compile_instancing
		#pragma surface surf Unlit keepalpha noshadow 
		struct Input
		{
			fixed filler;
		};

		UNITY_INSTANCING_BUFFER_START(IntersectIncr)
			UNITY_DEFINE_INSTANCED_PROP(float4, _Color)
#define _Color_arr IntersectIncr
		UNITY_INSTANCING_BUFFER_END(IntersectIncr)

		inline fixed4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return fixed4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 _Color_Instance = UNITY_ACCESS_INSTANCED_PROP(_Color_arr, _Color);
			o.Emission = _Color_Instance.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14101
873;89;1906;1103;1331.116;161.5525;1.179648;True;True
Node;AmplifyShaderEditor.ColorNode;1;-126.0668,25.52387;Float;False;InstancedProperty;_Color;Color;0;0;Create;1,0.5704868,0.1102941,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;276.5,-10.9;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;Intersect Incr;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;Front;2;7;False;1;0;Custom;0.5;True;False;3003;True;Transparent;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;2;2;2;5;1;1;3;0;0;0;0;False;2;15;10;25;False;0.5;False;0;SrcAlpha;OneMinusSrcAlpha;0;One;One;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;1;-1;-1;-1;0;0;0;False;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;0;2;1;0
ASEEND*/
//CHKSM=4B08802BDCDF511811F5B24185BB5A87000533CE