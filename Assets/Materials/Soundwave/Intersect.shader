// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Intersect"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+3002" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		ZWrite Off
		Offset  0 , 0
		Stencil
		{
			Ref 1
			ReadMask 2
			WriteMask 2
			Comp Always
			Pass Keep
			Fail Keep
			ZFail Replace
		}
		ColorMask 0
		CGPROGRAM
		#pragma target 5.0
		#pragma surface surf Unlit keepalpha noshadow 
		struct Input
		{
			fixed filler;
		};

		inline fixed4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return fixed4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 _Color0 = float4(0,0,0,0);
			o.Emission = _Color0.rgb;
			o.Alpha = _Color0.a;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14101
1120;215;1906;1103;183.9899;506.485;1;True;False
Node;AmplifyShaderEditor.ColorNode;21;202.4077,-172.6583;Float;False;Constant;_Color0;Color 0;0;0;Create;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;20;595.282,-213.0677;Float;False;True;7;Float;ASEMaterialInspector;0;0;Unlit;Intersect;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;Back;2;0;True;0;0;Custom;0.5;True;False;3002;True;Transparent;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;False;False;False;False;True;1;2;2;7;1;1;3;7;1;1;4;False;2;15;10;25;False;0.5;False;0;One;One;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;0;0;False;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;20;2;21;0
WireConnection;20;9;21;4
ASEEND*/
//CHKSM=3BB34FFB3674B931D8A1D1BFABA334CEA2B90C2F