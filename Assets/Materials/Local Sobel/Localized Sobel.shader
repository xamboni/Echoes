// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "PostProcess/Localized Sobel"
{
	Properties
	{
		_DepthIntensity("Depth Intensity", Float) = 1
		[Header(Normal Depth Combine)]
		[Header(Edge Detect)]
		_DepthMaxDistance("Depth Max Distance", Float) = 10000
		[Toggle(_USEFLATCOLOR_ON)] _UseFlatColor("Use Flat Color", Float) = 1
		_NormalMaxDistance("Normal Max Distance", Float) = 10000
		_NormalIntensity("Normal Intensity", Float) = 1
		_Hardness("Hardness", Float) = 200
		_SceneColor("Scene Color", Color) = (0,0,0,1)
		_EdgeColor("Edge Color", Color) = (1,1,1,1)
		_EdgePower("Edge Power", Float) = 5
		_EdgeSize("Edge Size", Float) = 1
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+1000" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Front
		ZWrite Off
		ZTest Always
		Blend SrcAlpha OneMinusSrcAlpha
		GrabPass{ }
		CGPROGRAM
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#pragma target 3.5
		#pragma shader_feature _USEFLATCOLOR_ON
		#pragma surface surf Unlit keepalpha noshadow vertex:vertexDataFunc 
		struct Input
		{
			float4 screenPos;
			float eyeDepth;
			float3 worldPos;
		};

		uniform sampler2D _GrabTexture;
		uniform float4 _SceneColor;
		uniform float4 _EdgeColor;
		uniform sampler2D _CameraDepthNormalsTexture;
		uniform float _EdgeSize;
		uniform sampler2D _CameraDepthTexture;
		uniform float _NormalMaxDistance;
		uniform float _NormalIntensity;
		uniform float _DepthMaxDistance;
		uniform float _DepthIntensity;
		uniform float _EdgePower;
		uniform float _Hardness;


		inline float4 ASE_ComputeGrabScreenPos( float4 pos )
		{
			#if UNITY_UV_STARTS_AT_TOP
			float scale = -1.0;
			#else
			float scale = 1.0;
			#endif
			float4 o = pos;
			o.y = pos.w * 0.5f;
			o.y = ( pos.y - o.y ) * _ProjectionParams.x * scale + o.y;
			return o;
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			o.eyeDepth = -UnityObjectToViewPos( v.vertex.xyz ).z;
		}

		inline fixed4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return fixed4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_grabScreenPos = ASE_ComputeGrabScreenPos( ase_screenPos );
			float4 screenColor35_g1 = tex2Dproj( _GrabTexture, UNITY_PROJ_COORD( ase_grabScreenPos ) );
			#ifdef _USEFLATCOLOR_ON
				float4 staticSwitch38_g1 = _SceneColor;
			#else
				float4 staticSwitch38_g1 = screenColor35_g1;
			#endif
			float2 appendResult5_g1 = (float2(( _ScreenParams.z - 1 ) , ( _ScreenParams.w - 1 )));
			float2 temp_output_1_0_g87 = ( appendResult5_g1 * _EdgeSize );
			float4 ase_grabScreenPosNorm = ase_grabScreenPos / ase_grabScreenPos.w;
			float2 temp_output_31_0_g87 = (ase_grabScreenPosNorm).xy;
			float2 temp_output_1_0_g82 = ( ( float2( 1,-1 ) * temp_output_1_0_g87 ) + temp_output_31_0_g87 );
			float depthDecodedVal62_g82 = 0;
			float3 normalDecodedVal62_g82 = float3(0,0,0);
			DecodeDepthNormal( tex2D( _CameraDepthNormalsTexture, temp_output_1_0_g82 ), depthDecodedVal62_g82, normalDecodedVal62_g82 );
			float temp_output_20_0_g82 = log2( tex2D( _CameraDepthTexture, temp_output_1_0_g82 ).r );
			float temp_output_21_0_g82 = log2( _NormalMaxDistance );
			float clampResult23_g82 = clamp( temp_output_20_0_g82 , 0 , temp_output_21_0_g82 );
			float3 lerpResult33_g82 = lerp( float3(0,0,0) , normalDecodedVal62_g82 , ( ( 1.0 - ( clampResult23_g82 / temp_output_21_0_g82 ) ) * _NormalIntensity ));
			float temp_output_22_0_g82 = log2( _DepthMaxDistance );
			float clampResult24_g82 = clamp( temp_output_20_0_g82 , 0 , temp_output_22_0_g82 );
			float3 appendResult39_g82 = (float3((lerpResult33_g82).xy , ( ( 1.0 - ( clampResult24_g82 / temp_output_22_0_g82 ) ) * ( _DepthIntensity * ( temp_output_22_0_g82 - temp_output_20_0_g82 ) ) )));
			float3 temp_output_398_0_g1 = appendResult39_g82;
			float3 break15_g73 = float3(1,2,1);
			float2 temp_output_1_0_g80 = ( ( float2( 0,-1 ) * temp_output_1_0_g87 ) + temp_output_31_0_g87 );
			float depthDecodedVal62_g80 = 0;
			float3 normalDecodedVal62_g80 = float3(0,0,0);
			DecodeDepthNormal( tex2D( _CameraDepthNormalsTexture, temp_output_1_0_g80 ), depthDecodedVal62_g80, normalDecodedVal62_g80 );
			float temp_output_20_0_g80 = log2( tex2D( _CameraDepthTexture, temp_output_1_0_g80 ).r );
			float temp_output_21_0_g80 = log2( _NormalMaxDistance );
			float clampResult23_g80 = clamp( temp_output_20_0_g80 , 0 , temp_output_21_0_g80 );
			float3 lerpResult33_g80 = lerp( float3(0,0,0) , normalDecodedVal62_g80 , ( ( 1.0 - ( clampResult23_g80 / temp_output_21_0_g80 ) ) * _NormalIntensity ));
			float temp_output_22_0_g80 = log2( _DepthMaxDistance );
			float clampResult24_g80 = clamp( temp_output_20_0_g80 , 0 , temp_output_22_0_g80 );
			float3 appendResult39_g80 = (float3((lerpResult33_g80).xy , ( ( 1.0 - ( clampResult24_g80 / temp_output_22_0_g80 ) ) * ( _DepthIntensity * ( temp_output_22_0_g80 - temp_output_20_0_g80 ) ) )));
			float2 temp_output_1_0_g86 = ( ( float2( -1,-1 ) * temp_output_1_0_g87 ) + temp_output_31_0_g87 );
			float depthDecodedVal62_g86 = 0;
			float3 normalDecodedVal62_g86 = float3(0,0,0);
			DecodeDepthNormal( tex2D( _CameraDepthNormalsTexture, temp_output_1_0_g86 ), depthDecodedVal62_g86, normalDecodedVal62_g86 );
			float temp_output_20_0_g86 = log2( tex2D( _CameraDepthTexture, temp_output_1_0_g86 ).r );
			float temp_output_21_0_g86 = log2( _NormalMaxDistance );
			float clampResult23_g86 = clamp( temp_output_20_0_g86 , 0 , temp_output_21_0_g86 );
			float3 lerpResult33_g86 = lerp( float3(0,0,0) , normalDecodedVal62_g86 , ( ( 1.0 - ( clampResult23_g86 / temp_output_21_0_g86 ) ) * _NormalIntensity ));
			float temp_output_22_0_g86 = log2( _DepthMaxDistance );
			float clampResult24_g86 = clamp( temp_output_20_0_g86 , 0 , temp_output_22_0_g86 );
			float3 appendResult39_g86 = (float3((lerpResult33_g86).xy , ( ( 1.0 - ( clampResult24_g86 / temp_output_22_0_g86 ) ) * ( _DepthIntensity * ( temp_output_22_0_g86 - temp_output_20_0_g86 ) ) )));
			float3 temp_output_393_0_g1 = appendResult39_g86;
			float3 break16_g73 = float3(0,0,0);
			float2 temp_output_1_0_g81 = ( ( float2( 1,1 ) * temp_output_1_0_g87 ) + temp_output_31_0_g87 );
			float depthDecodedVal62_g81 = 0;
			float3 normalDecodedVal62_g81 = float3(0,0,0);
			DecodeDepthNormal( tex2D( _CameraDepthNormalsTexture, temp_output_1_0_g81 ), depthDecodedVal62_g81, normalDecodedVal62_g81 );
			float temp_output_20_0_g81 = log2( tex2D( _CameraDepthTexture, temp_output_1_0_g81 ).r );
			float temp_output_21_0_g81 = log2( _NormalMaxDistance );
			float clampResult23_g81 = clamp( temp_output_20_0_g81 , 0 , temp_output_21_0_g81 );
			float3 lerpResult33_g81 = lerp( float3(0,0,0) , normalDecodedVal62_g81 , ( ( 1.0 - ( clampResult23_g81 / temp_output_21_0_g81 ) ) * _NormalIntensity ));
			float temp_output_22_0_g81 = log2( _DepthMaxDistance );
			float clampResult24_g81 = clamp( temp_output_20_0_g81 , 0 , temp_output_22_0_g81 );
			float3 appendResult39_g81 = (float3((lerpResult33_g81).xy , ( ( 1.0 - ( clampResult24_g81 / temp_output_22_0_g81 ) ) * ( _DepthIntensity * ( temp_output_22_0_g81 - temp_output_20_0_g81 ) ) )));
			float3 temp_output_399_0_g1 = appendResult39_g81;
			float3 break17_g73 = float3(-1,-2,-1);
			float2 temp_output_1_0_g83 = ( ( float2( 0,1 ) * temp_output_1_0_g87 ) + temp_output_31_0_g87 );
			float depthDecodedVal62_g83 = 0;
			float3 normalDecodedVal62_g83 = float3(0,0,0);
			DecodeDepthNormal( tex2D( _CameraDepthNormalsTexture, temp_output_1_0_g83 ), depthDecodedVal62_g83, normalDecodedVal62_g83 );
			float temp_output_20_0_g83 = log2( tex2D( _CameraDepthTexture, temp_output_1_0_g83 ).r );
			float temp_output_21_0_g83 = log2( _NormalMaxDistance );
			float clampResult23_g83 = clamp( temp_output_20_0_g83 , 0 , temp_output_21_0_g83 );
			float3 lerpResult33_g83 = lerp( float3(0,0,0) , normalDecodedVal62_g83 , ( ( 1.0 - ( clampResult23_g83 / temp_output_21_0_g83 ) ) * _NormalIntensity ));
			float temp_output_22_0_g83 = log2( _DepthMaxDistance );
			float clampResult24_g83 = clamp( temp_output_20_0_g83 , 0 , temp_output_22_0_g83 );
			float3 appendResult39_g83 = (float3((lerpResult33_g83).xy , ( ( 1.0 - ( clampResult24_g83 / temp_output_22_0_g83 ) ) * ( _DepthIntensity * ( temp_output_22_0_g83 - temp_output_20_0_g83 ) ) )));
			float2 temp_output_1_0_g79 = ( ( float2( -1,1 ) * temp_output_1_0_g87 ) + temp_output_31_0_g87 );
			float depthDecodedVal62_g79 = 0;
			float3 normalDecodedVal62_g79 = float3(0,0,0);
			DecodeDepthNormal( tex2D( _CameraDepthNormalsTexture, temp_output_1_0_g79 ), depthDecodedVal62_g79, normalDecodedVal62_g79 );
			float temp_output_20_0_g79 = log2( tex2D( _CameraDepthTexture, temp_output_1_0_g79 ).r );
			float temp_output_21_0_g79 = log2( _NormalMaxDistance );
			float clampResult23_g79 = clamp( temp_output_20_0_g79 , 0 , temp_output_21_0_g79 );
			float3 lerpResult33_g79 = lerp( float3(0,0,0) , normalDecodedVal62_g79 , ( ( 1.0 - ( clampResult23_g79 / temp_output_21_0_g79 ) ) * _NormalIntensity ));
			float temp_output_22_0_g79 = log2( _DepthMaxDistance );
			float clampResult24_g79 = clamp( temp_output_20_0_g79 , 0 , temp_output_22_0_g79 );
			float3 appendResult39_g79 = (float3((lerpResult33_g79).xy , ( ( 1.0 - ( clampResult24_g79 / temp_output_22_0_g79 ) ) * ( _DepthIntensity * ( temp_output_22_0_g79 - temp_output_20_0_g79 ) ) )));
			float3 temp_output_397_0_g1 = appendResult39_g79;
			float3 temp_output_365_0_g1 = ( ( ( ( temp_output_398_0_g1 * break15_g73.z ) + ( ( appendResult39_g80 * break15_g73.y ) + ( temp_output_393_0_g1 * break15_g73.x ) ) ) + ( ( float3( 0,0,0 ) * break16_g73.z ) + ( ( float3( 0,0,0 ) * break16_g73.y ) + ( float3( 0,0,0 ) * break16_g73.x ) ) ) ) + ( ( temp_output_399_0_g1 * break17_g73.z ) + ( ( appendResult39_g83 * break17_g73.y ) + ( temp_output_397_0_g1 * break17_g73.x ) ) ) );
			float3 break15_g88 = float3(1,0,-1);
			float2 temp_output_1_0_g84 = ( ( float2( 1,0 ) * temp_output_1_0_g87 ) + temp_output_31_0_g87 );
			float depthDecodedVal62_g84 = 0;
			float3 normalDecodedVal62_g84 = float3(0,0,0);
			DecodeDepthNormal( tex2D( _CameraDepthNormalsTexture, temp_output_1_0_g84 ), depthDecodedVal62_g84, normalDecodedVal62_g84 );
			float temp_output_20_0_g84 = log2( tex2D( _CameraDepthTexture, temp_output_1_0_g84 ).r );
			float temp_output_21_0_g84 = log2( _NormalMaxDistance );
			float clampResult23_g84 = clamp( temp_output_20_0_g84 , 0 , temp_output_21_0_g84 );
			float3 lerpResult33_g84 = lerp( float3(0,0,0) , normalDecodedVal62_g84 , ( ( 1.0 - ( clampResult23_g84 / temp_output_21_0_g84 ) ) * _NormalIntensity ));
			float temp_output_22_0_g84 = log2( _DepthMaxDistance );
			float clampResult24_g84 = clamp( temp_output_20_0_g84 , 0 , temp_output_22_0_g84 );
			float3 appendResult39_g84 = (float3((lerpResult33_g84).xy , ( ( 1.0 - ( clampResult24_g84 / temp_output_22_0_g84 ) ) * ( _DepthIntensity * ( temp_output_22_0_g84 - temp_output_20_0_g84 ) ) )));
			float3 break16_g88 = float3(2,0,-2);
			float2 temp_output_1_0_g85 = ( ( float2( -1,0 ) * temp_output_1_0_g87 ) + temp_output_31_0_g87 );
			float depthDecodedVal62_g85 = 0;
			float3 normalDecodedVal62_g85 = float3(0,0,0);
			DecodeDepthNormal( tex2D( _CameraDepthNormalsTexture, temp_output_1_0_g85 ), depthDecodedVal62_g85, normalDecodedVal62_g85 );
			float temp_output_20_0_g85 = log2( tex2D( _CameraDepthTexture, temp_output_1_0_g85 ).r );
			float temp_output_21_0_g85 = log2( _NormalMaxDistance );
			float clampResult23_g85 = clamp( temp_output_20_0_g85 , 0 , temp_output_21_0_g85 );
			float3 lerpResult33_g85 = lerp( float3(0,0,0) , normalDecodedVal62_g85 , ( ( 1.0 - ( clampResult23_g85 / temp_output_21_0_g85 ) ) * _NormalIntensity ));
			float temp_output_22_0_g85 = log2( _DepthMaxDistance );
			float clampResult24_g85 = clamp( temp_output_20_0_g85 , 0 , temp_output_22_0_g85 );
			float3 appendResult39_g85 = (float3((lerpResult33_g85).xy , ( ( 1.0 - ( clampResult24_g85 / temp_output_22_0_g85 ) ) * ( _DepthIntensity * ( temp_output_22_0_g85 - temp_output_20_0_g85 ) ) )));
			float3 break17_g88 = float3(1,0,-1);
			float3 temp_output_364_0_g1 = ( ( ( ( temp_output_398_0_g1 * break15_g88.z ) + ( ( float3( 0,0,0 ) * break15_g88.y ) + ( temp_output_393_0_g1 * break15_g88.x ) ) ) + ( ( appendResult39_g84 * break16_g88.z ) + ( ( float3( 0,0,0 ) * break16_g88.y ) + ( appendResult39_g85 * break16_g88.x ) ) ) ) + ( ( temp_output_399_0_g1 * break17_g88.z ) + ( ( float3( 0,0,0 ) * break17_g88.y ) + ( temp_output_397_0_g1 * break17_g88.x ) ) ) );
			float clampResult32_g1 = clamp( length( ( ( temp_output_365_0_g1 * temp_output_365_0_g1 ) + ( temp_output_364_0_g1 * temp_output_364_0_g1 ) ) ) , 0 , 1 );
			float4 lerpResult40_g1 = lerp( staticSwitch38_g1 , _EdgeColor , pow( clampResult32_g1 , _EdgePower ));
			float4 temp_output_186_0 = lerpResult40_g1;
			o.Emission = temp_output_186_0.rgb;
			float eyeDepth10 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD(ase_screenPos))));
			float4 transform3_g431 = mul(unity_ObjectToWorld,float4(0,0,0,1));
			float3 temp_output_3_0 = (transform3_g431).xyz;
			float3 ase_worldPos = i.worldPos;
			float4 transform8 = mul(unity_WorldToObject,float4( ( temp_output_3_0 - ase_worldPos ) , 0.0 ));
			float4 transform9 = mul(unity_WorldToObject,float4( ( temp_output_3_0 - _WorldSpaceCameraPos ) , 0.0 ));
			float4 temp_output_5_0_g555 = ( ( ( ( ( eyeDepth10 / i.eyeDepth ) * ( transform8 - transform9 ) ) + transform9 ) - float4( float3(0,0,0) , 0.0 ) ) / 0.5 );
			float dotResult19_g555 = dot( temp_output_5_0_g555 , temp_output_5_0_g555 );
			float clampResult10_g555 = clamp( dotResult19_g555 , 0 , 1 );
			o.Alpha = ( (temp_output_186_0).a * ( 1.0 - pow( clampResult10_g555 , _Hardness ) ) );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15201
259;556;1785;1001;1722.412;495.2303;1.3;True;True
Node;AmplifyShaderEditor.FunctionNode;157;-2810.85,182.5337;Float;False;Actor Position;-1;;431;99bf3a5126f6234469ac9003f655b998;0;0;1;FLOAT4;0
Node;AmplifyShaderEditor.ComponentMaskNode;3;-2593.023,178.5276;Float;False;True;True;True;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldPosInputsNode;5;-2653.85,13.5285;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceCameraPos;4;-2718.698,268.8859;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleSubtractOpNode;7;-2247.179,268.4414;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;6;-2252.061,61.77703;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SurfaceDepthNode;11;-2057.716,-38.12455;Float;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenDepthNode;10;-2056.542,-103.0589;Float;False;0;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldToObjectTransfNode;9;-2080.209,268.8368;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldToObjectTransfNode;8;-2075.209,62.8371;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;13;-1613.569,294.6469;Float;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;12;-1791.462,147.0302;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;14;-1781.183,-61.75066;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;16;-1476.903,252.3938;Float;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;-1618.085,27.75169;Float;False;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.CommentaryNode;15;-1239.106,-104.9074;Float;False;820.4473;581.0168;Performs the actual intersection masking;5;20;19;18;171;176;Masking;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;21;-1400.224,29.88969;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-1186.127,396.2719;Float;False;Property;_Hardness;Hardness;3;0;Create;True;0;0;False;0;200;200;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;20;-1188.252,326.0332;Float;False;Constant;_Float2;Float 2;2;0;Create;True;0;0;False;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;18;-1196.38,184.2342;Float;False;Constant;_Vector1;Vector 1;3;0;Create;True;0;0;False;0;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;178;-595.314,-395.78;Float;False;Property;_EdgePower;Edge Power;6;0;Create;True;0;0;False;0;5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;177;-603.1138,-241.0803;Float;False;Property;_EdgeSize;Edge Size;7;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;161;-856.2302,-579.3093;Float;False;Property;_EdgeColor;Edge Color;5;0;Create;True;0;0;False;0;1,1,1,1;1,1,1,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;160;-847.4408,-373.2803;Float;False;Property;_SceneColor;Scene Color;4;0;Create;True;0;0;False;0;0,0,0,1;0,0,0,1;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;186;-421.9674,-397.5642;Float;False;Edge Detect;0;;1;e6582b104f93bac419a41ed6c258e046;0;4;34;FLOAT;5;False;41;COLOR;1,1,1,0;False;39;COLOR;1,1,1,0;False;4;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;171;-976.2167,2.094553;Float;False;Sphere Mask New;-1;;555;e0315a3b9e99b3e4ab8cb9813b2fba90;0;4;16;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;14;FLOAT;0;False;12;FLOAT;100;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;176;-581.0146,9.819654;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;180;-327.5147,-207.2804;Float;False;False;False;False;True;1;0;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;179;-204.0148,178.8197;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;36.49998,-115.6;Float;False;True;3;Float;ASEMaterialInspector;0;0;Unlit;PostProcess/Localized Sobel;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;Front;2;False;-1;7;False;-1;False;0;0;False;0;Custom;0.5;True;False;1000;True;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;2;False;-1;10;False;-1;10;False;-1;7;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;-1;False;-1;-1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;2;-1;-1;-1;0;0;0;False;0;0;0;False;-1;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;3;0;157;0
WireConnection;7;0;3;0
WireConnection;7;1;4;0
WireConnection;6;0;3;0
WireConnection;6;1;5;0
WireConnection;9;0;7;0
WireConnection;8;0;6;0
WireConnection;13;0;9;0
WireConnection;12;0;8;0
WireConnection;12;1;9;0
WireConnection;14;0;10;0
WireConnection;14;1;11;0
WireConnection;16;0;13;0
WireConnection;17;0;14;0
WireConnection;17;1;12;0
WireConnection;21;0;17;0
WireConnection;21;1;16;0
WireConnection;186;34;178;0
WireConnection;186;41;161;0
WireConnection;186;39;160;0
WireConnection;186;4;177;0
WireConnection;171;16;21;0
WireConnection;171;15;18;0
WireConnection;171;14;20;0
WireConnection;171;12;19;0
WireConnection;176;0;171;0
WireConnection;180;0;186;0
WireConnection;179;0;180;0
WireConnection;179;1;176;0
WireConnection;0;2;186;0
WireConnection;0;9;179;0
ASEEND*/
//CHKSM=364FAB2D36337C675C3060F5594D8A2E0201C1BA