﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour
{
	private Texture2D fadeTexture;
	private float alpha = 0f;

	// Use this for initialization
	void Start()
	{
		
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			GameManager.Instance.HasWon = true;
		}
	}

	float scaleHeight(float number)
	{
		return (number / 1080f) * Screen.height;
	}

	Vector2 DrawText(string text, int fontSize, float x, float y, Color color, TextAnchor alignment, int justification)
	{
		// what is this crap??
		GUIContent content = new GUIContent(text);
		GUIStyle style = new GUIStyle("label");
		style.alignment = alignment;
		style.normal.textColor = color;
		style.fontSize = fontSize;
		style.normal.background = null;

		// give me my text size
		Vector2 textSize = style.CalcSize(content);

		// manual text justification because anchors DON'T DO ANYTHING
		if (justification == 1) // centered
			GUI.Label(new Rect(x - (textSize.x / 2), y, textSize.x, textSize.y), text, style);
		else if (justification == 2) // right-justified
			GUI.Label(new Rect(x - textSize.x, y, textSize.x, textSize.y), text, style);
		else // regular
			GUI.Label(new Rect(x, y, textSize.x, textSize.y), text, style);

		// give the text size back, just in case...
		return textSize;
	}

	private void OnGUI()
	{
		if (!GameManager.Instance.HasWon) return;

		alpha += 0.2f * Time.deltaTime;  
		alpha = Mathf.Clamp01(alpha);  
         
		GUI.color = new Color(0, 0, 0, alpha);
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeTexture);
	}

	// Update is called once per frame
	void Update()
	{
		
	}
}
